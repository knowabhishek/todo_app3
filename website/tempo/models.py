from django.db import models

# Create your models here.

class Tempo(models.Model):
    f_id = models.CharField(max_length=10)
    src = models.CharField(max_length=10)
    dest = models.CharField(max_length=10)
    fare = models.IntegerField()

    def __repr__(self):
        return """
    Tempo ID: {}
    Source: {}
    Destination: {}
    Fare: {}
    """.format(self.f_id, self.src, self.dest, self.fare)

    def __str__(self):
        return self.f_id

