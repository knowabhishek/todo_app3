from django.shortcuts import render
from django.http import HttpResponse

from .models import Tempo
# Create your views here.


def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def booking(request):
    all_records = Tempo.objects.all()
    context = {'records': all_records}

    return render(request, 'booking.html', context)

